# opengeogame

Open Geo Game. Place the cities on the map and earn points, if it is enough close.

An instance of this game might be running at [https://geo.games.s1gm4.eu](https://geo.games.s1gm4.eu).

## Installation

```bash
git clone https://framagit.org/1ibre/games/GeoGame.git
cd GeoGame
npm install
```

## Usage

```bash
npm start
```
