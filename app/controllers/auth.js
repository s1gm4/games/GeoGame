const bcrypt = require("bcrypt");
const db = require("../db");
const User = require("../models/user")(db);

function redirectToLogin(req, res) {
  res.redirect("/auth/login");
}

function loginpage(req, res) {
  res.render('auth/login', {
    title: "OpenGeoGame - Login",
    user: req.session.username,
    csrf_token: req.csrfToken()
  });
}

function login(req, res) {
  const { username, password } = req.body;
  if (username === "" || password == "") {
    res.render('auth/login', {
      message: "Indicate username and password",
      csrf_token: req.csrfToken()
    });
    return;
  } else {
    User.findOne({
      where:
      {
        username
      }
    })
      .then(user => {
        if (!user) {
          res.render('auth/login', {
            message: "The username doesn't exist",
            csrf_token: req.csrfToken()
          });
          return;
        } else if (bcrypt.compareSync(password, user.password)) {
          req.session.username = user.username;
          res.redirect("/");
        } else {
          res.render('auth/login', {
            message: "Incorrect password",
            csrf_token: req.csrfToken()
          });
        }
      })
      .catch(error => {
        console.error(error);
      })
  }
}

function registerpage(req, res) {
  res.render('auth/register', {
    title: "OpenGeoGame - Register",
    user: req.session.username,
    csrf_token: req.csrfToken()
  });
}

function register(req, res) {
  if (req.body.username === "" || req.body.password === "") {
    res.render('auth/register', { message: "Please fill a username and password" });
    return;
  } else {
    User.findOne({
      where: {
        username: req.body.username
      }
    }).then(user => {
      if (user !== null) {
        res.render('auth/register', {
          message: "The username already exists, please find another",
          user: req.session.username,
          csrf_token: req.csrfToken()
        });
        return;
      } else {
        const salt = bcrypt.genSaltSync(10);
        const hashPass = bcrypt.hashSync(req.body.password, salt);
        const newUser = new User({
          username: req.body.username,
          password: hashPass
        });
        newUser.save()
          .then(() => {
            req.session.username = newUser.username;
            res.redirect("/");
          })
          .catch(error => {
            console.error(error);
            res.render('auth/register', {
              message: "Something went wrong",
              user: req.session.username,
              csrf_token: req.csrfToken()
            });
          })
      }
    })
      .catch(error => {
        console.error(error);
        res.render('auth/register', {
          message: "Something went wrong",
          user: req.session.username,
          csrf_token: req.csrfToken()
        });
      })
  }
}

module.exports = {
  redirectToLogin,
  loginpage,
  login,
  registerpage,
  register
}
