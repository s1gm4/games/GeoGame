function index(req, res) {
    res.render('index', {
      title: 'OpenGeoGame',
      user: req.session.username,
      csrf_token: req.csrfToken()
    });
}

function game(req, res) {
    res.render('game', {
      title: 'OpenGeoGame - Play',
      user: req.session.username,
      csrf_token: req.csrfToken()
  });
}

function about(req, res) {
  res.render('about', {
    title: 'OpenGeoGame - About',
    user: req.session.username
  });
}

module.exports = {
    index,
    about,
    game
}
