const sequelize = require('sequelize');
const DATABASE_URI = process.env.DATABASE_URI || 'sqlite:memory';
const db = new sequelize(DATABASE_URI);

module.exports = db;
