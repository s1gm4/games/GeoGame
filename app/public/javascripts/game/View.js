const View = {
    start: $('.start'),
    map: $('#map'),
    city: $('#city'),
    message: $('#message'),
    setCity(city) {
      this.city.text(city);
    },
    setMessage(message) {
      this.message.text(message);
    }
}

export default View;
