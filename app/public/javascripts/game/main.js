import View from "./View.js";
import Map from "./map.js";
import API from "./api.js";

let marked = false;
const logged = $('.logged').length ? true : false;

View.start.on('click', start);

View.map.on('click', function (e) {
  if (logged) {
    const { lat, lng } = Map.getCoords()
    check(lat, lng);
  } else {
    View.message.text('You need to be logged in to play');
  }
});

function start() {
  if (logged) {
    Map.clear();
    marked = false;
    city();
  } else {
    View.message.text('You must be logged in to play');
  }
}

function city() {
  API.getCity().then((res) => {
    if (res.status == 401) {
      View.message.text('You must be logged in to play');
    } else {
      const city = res.data;
      View.setCity(city.name);
    }
  }).catch((err) => {
    console.error(err);
  });
}

function check(lat, lng) {
  if (!marked) {
    API.checkCoords([lat, lng], View.city.text()).then(res => {
      const { dist, coords } = res.data;
      Map.setCircle(coords, dist);
      let message = `You are ${Math.round(dist)} km away from ${View.city.text()}`;
      let points = Math.round(1000 / dist);
      View.message.text(`${message}: You got ${points} points.`);
      marked = true;
    }).catch(err => {
      console.error(err);
    });
  }
}
