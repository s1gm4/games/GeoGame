const express = require('express');
const router = express.Router();
const controller = require('../controllers/api');

router.route('/')
        .get(controller.docs);

router.route('/city/random')
        .get(controller.getCity);

router.route('/city/check')
        .post(controller.checkCoordinates);

module.exports = router;
