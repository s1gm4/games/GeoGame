const express = require('express');
const router = express.Router();
const controller = require('../controllers/auth');

router.route('/')
    .get(controller.redirectToLogin);

router.route('/login')
      .get(controller.loginpage)
      .post(controller.login);

router.route('/register')
      .get(controller.registerpage)
      .post(controller.register);

module.exports = router;
