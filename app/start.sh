#! /usr/bin/env bash
# Start docker container

CONTAINER_ALREADY_STARTED="started"

if [[ ! -e $CONTAINER_ALREADY_STARTED ]]; then
    echo "=> First container startup ..."
    touch $CONTAINER_ALREADY_STARTED
    npx sequelize-cli db:migrate
else
    echo "=> Container alreay started once ..."
fi

npm start
